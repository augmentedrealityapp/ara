﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AraPipe
{

    // Private Objects
    public Vector3 StartPos { get; set; }
    public Vector3 EndPos { get; set; }
    public GameObject GameObject { get; set; }

    public float distanceBetween
    {
        get
        {
            if (StartPos != null && EndPos != null)
                return Vector3.Distance(StartPos, EndPos);
            else
                return 0;
        }
    }

    public Vector3 midPos
    {
        get
        {
            return ((EndPos - StartPos) * 0.5f) + StartPos;
        }
    }

    public AraPipe(Vector3 startPos, Vector3 endPos)
    {
        this.StartPos = startPos;
        this.EndPos = endPos;
    }
}

