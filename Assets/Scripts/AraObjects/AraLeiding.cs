﻿using System.Collections.Generic;

class AraLeiding
{
    public string displayFieldName;
    public AraFieldAliases fieldAliases;
    public string geometryType;
    public AraSpatialReference spatialReference;
    public List<AraField> fields;
    public List<AraFeature> features;
}