﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test_Rotator : MonoBehaviour {

    int rotateSpeed = 25;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        gameObject.transform.Rotate(Vector3.up * (rotateSpeed * Time.deltaTime));
	}
}
