﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GeoCalcHelper {
    private static float _earthRadius = 6371.1F;

    public static float CalculateX(float longitudeReference, float longitudePipe, float latitudeReference)
    {
        return _earthRadius * (longitudeReference - longitudePipe) * Mathf.Cos(latitudeReference);
    }

    public static float CalculateY(float latidudeReference, float latitude)
    {
        return _earthRadius * (latidudeReference - latitude);
    }
}