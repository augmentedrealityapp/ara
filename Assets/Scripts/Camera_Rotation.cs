﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Rotation : MonoBehaviour {


    public GameObject Camera;

    private float _objX;
    private float _objY;
    private float _objZ;

    
    void Start()
    {
        Input.gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        var cubeX = Input.gyro.rotationRateUnbiased.x;
        var cubeY = Input.gyro.rotationRateUnbiased.y;
        var cubeZ = Input.gyro.rotationRateUnbiased.z;

        Camera.transform.Rotate(-cubeX, -cubeY, cubeZ);
       
        //Debug.Log(_objX);
    }
}
