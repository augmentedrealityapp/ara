﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using System.Net;
using System;
using System.Threading;

public class Asset_Orientation : MonoBehaviour
{
    public GameObject[] _positions;
    public GameObject dummyPipe;

    public double longitudeReference ;
    public double latitudeReference ;

    public bool assetsLoaded = false;
    public bool serverConnectionEstablished = false;

    void Start()
    {
        WWW www = new WWW(AraWebConstant.FULL_URL);
        StartCoroutine(WaitForRequest(www));

        while (!www.isDone)
            Thread.Sleep(500);

        serverConnectionEstablished = true;
        HandleJsonData(www.text);
    }

    void Update()
    {

    }

    IEnumerator WaitForRequest(WWW www)
    {
            yield return www;  //this is where your code break.
    }


    private void HandleJsonData(string json)
    {
        AraLeiding leiding = JsonConvert.DeserializeObject<AraLeiding>(json);

        List<AraFeature> f = leiding.features;

        for (int i = 0; i < f.Count; i++)
        {
            List<List<List<double>>> coordinates = leiding.features[i].geometry.paths;

            List<Vector3> pipePositionsInVector3 = HandleCoordinates(coordinates);
            GameObject[] pipePostionsInGameObjects = CreatePipePoints(pipePositionsInVector3);
            List<AraPipe> araPipes = CreatePipes(pipePostionsInGameObjects);
            DrawPipes(araPipes);
        }
    }

    private GameObject[] CreatePipePoints(List<Vector3> Vector3List)
    {
        GameObject[] PipePoints = new GameObject[Vector3List.Count];

        for(int i = 0; i < Vector3List.Count; i ++)
        {
            GameObject cube = new GameObject();
            cube.transform.position = Vector3List[i];
            PipePoints[i] = cube;
        }

        return PipePoints;
    }

    private List<Vector3> HandleCoordinates(List<List<List<double>>> coordinates)
    {
        List<Vector3> pipePositions = new List<Vector3>();

        latitudeReference = 52.2984358;
        longitudeReference = 4.8627616;

        foreach (List<double> geoPos in coordinates[0])
        {
            double pipeGeoLongitude = geoPos[0];
            double pipeGeoLatitude = geoPos[1];

            float xPos = GeoCalcHelper.CalculateX((float)longitudeReference, (float)pipeGeoLongitude, (float)latitudeReference);
            float yPos = GeoCalcHelper.CalculateY((float)latitudeReference, (float)pipeGeoLatitude);

            Vector3 pipePos = new Vector3(xPos, 0, yPos);
            pipePositions.Add(pipePos);
        }

        return pipePositions;
    }

    /// <summary>
    /// Create AraPipes from Vector Points in a list
    /// You can't create a pipe with 1 position. So do nothing when the first position is looped
    /// When the second postion is looped we get a;
    ///     StartPosition = previous position
    ///     EndPosition = current position
    /// Till the last position in the list
    /// </summary>
    /// <returns></returns>
    public List<AraPipe> CreatePipes()
    {
        List<AraPipe> araPipes = new List<AraPipe>();
        for (int i = 0; i < _positions.Length; i++)
        {
            // You can't create a pipe from the first position. Two positions are needed, so go the next GameObject
            if (i == 0)
                continue;

            // Create Pipe with positions from the previous and current GameObject
            araPipes.Add(new AraPipe(_positions[i - 1].transform.position, _positions[i].transform.position));
        }
        return araPipes;
    }

    public List<AraPipe> CreatePipes(GameObject[] _positionsFromService)
    {
        List<AraPipe> araPipes = new List<AraPipe>();
        for (int i = 0; i < _positionsFromService.Length; i++)
        {
            // You can't create a pipe from the first position. Two positions are needed, so go the next GameObject
            if (i == 0)
                continue;

            // Create Pipe with positions from the previous and current GameObject
            araPipes.Add(new AraPipe(_positionsFromService[i - 1].transform.position, _positionsFromService[i].transform.position));
        }
        return araPipes;
    }

    public void DrawPipes(List<AraPipe> araPipes)
    {
        int count = araPipes.Count;
        foreach (AraPipe araPipe in araPipes)
        {
            // cylinder = 3d opbject
            // pipe = vlak
            GameObject pipe = Instantiate(dummyPipe, araPipe.midPos, Quaternion.identity) as GameObject;
            Transform cylinder = pipe.transform.GetChild(0);
            cylinder.transform.localScale = new Vector3(0.1f, (araPipe.distanceBetween / 2), 0.1f);
            pipe.transform.LookAt(araPipe.EndPos);
        }
    }




    //Debug.Log(cube.transform.position);

    /* GetAssetData will get Vector 3 positions from the Asset_Generation script. 
    * The coordinate data from the .json file will pass through the coordinate to Vector3 position conversion.
    * The Vector3's will then be put into the Array in Asset_Generation.
    * Vector3's will be used to draw the correct Line
    * I only have to get the asset TYPE once since the .json file is drawn PER line and so all segments will have the same TYPE/Color
    * DISCUSS THE STRUCTURE OF THESE ARRAYS! SEE NOTES! */


    //startPos = new Vector3(0, -5, 25);
    //endPos = new Vector3(25, 10, 50);
    //pipe = Instantiate(pipe, midPos, Quaternion.identity) as GameObject;
    //cylinder.transform.localScale = new Vector3(1, (distanceBetween / 2), 1);






}