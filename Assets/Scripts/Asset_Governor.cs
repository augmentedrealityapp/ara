﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asset_Governor : MonoBehaviour {

    public int[] arrayOfActualLines;
    private int amountOfLinesToDraw;
    public int[] arrayOfPossibleLines;

    public Vector3 initialLineStartPos;

    public GameObject assetDrawer;

    // Use this for initialization
	void Start ()
    {
        //Asset_Orientation script = assetDrawer.GetComponent<Asset_Orientation>();

        foreach (int line in arrayOfActualLines)
        {
            initialLineStartPos = new Vector3(Random.Range(-2.0f, 2.0f), 0, Random.Range(-2.0f, 2.0f));
            Instantiate(assetDrawer, initialLineStartPos, Quaternion.identity);
            
        }

        Debug.Log(arrayOfPossibleLines.Length);
	}
	
	// Update is called once per frame
	void Update ()
    {
        Movement();
	}
    
    void Movement()
    {
        
    }
}
