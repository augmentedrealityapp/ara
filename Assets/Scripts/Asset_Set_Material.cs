﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asset_Set_Material : MonoBehaviour {

    Renderer r_cylinder;

    public int type ;
    // type 0 = Gas pipe = Red
    // type 1 = electical line = Green
    // type 2 =  Water pipe = Blue

    public Material[] materials;

    void Start ()
    {
        r_cylinder = this.gameObject.GetComponent<Renderer>();
        r_cylinder.material = null;
        AssignMaterial();
	}
	
	void Update ()
    {
		
	}

    void AssignMaterial()
    {
        if (type == 0)
        {
            //r_cylinder.material = null;
            r_cylinder.material = materials[0];
        }
        if (type == 1)
        {
            //r_cylinder.material = null;
            r_cylinder.material = materials[1];
        }
        if (type == 2)
        {
            //r_cylinder.material = null;
            r_cylinder.material = materials[2];
        }
    }
}
