﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasGetPublicVariables : MonoBehaviour {

    public GameObject _assetDrawer;

    double _Lat;
    double _Long;

    Asset_Orientation script;

    public Text latUIText;
    public Text longUIText;

    public RawImage serverConnectionPositive;
    public RawImage serverConnectionNegative;
    public RawImage assetsLoadedPositive;
    public RawImage assetsLoadedNegative;

    bool _assetsLoaded;
    bool _serverConnectionEstablished;

	// Use this for initialization
	void Start ()
    {
        script = _assetDrawer.GetComponent<Asset_Orientation>();
        _Lat  = script.latitudeReference;
        _Long = script.longitudeReference;
        _assetsLoaded = script.assetsLoaded;
        _serverConnectionEstablished = script.serverConnectionEstablished;

        serverConnectionPositive.enabled = false;
        assetsLoadedPositive.enabled = false;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        latUIText.text = _Lat.ToString();
        longUIText.text = _Long.ToString();

        Debug.Log(_Lat.ToString());
        Debug.Log(_Long.ToString());

        CheckAssetsLoaded();
        CheckServerConnectionEstablished();
	}

    void CheckServerConnectionEstablished()
    {
        if (_serverConnectionEstablished == true)
        {
            serverConnectionPositive.enabled = true;
            serverConnectionNegative.enabled = false;
        }
    }

    void CheckAssetsLoaded()
    {
        if (_assetsLoaded == true)
        {
            assetsLoadedPositive.enabled = true;
            assetsLoadedNegative.enabled = false;
        }
    }
}
